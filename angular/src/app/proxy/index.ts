import * as Authors from './authors';
import * as BookStore from './book-store';
import * as Books from './books';
export { Authors, BookStore, Books };
