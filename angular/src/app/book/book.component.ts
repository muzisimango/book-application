import { AuthorLookupDto } from './../proxy/books/models';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { Component, OnInit } from '@angular/core';
import { BookService, BookDto, bookTypeOptions} from '@proxy/book-store/books';  //add bookTypeOptions to BookStore
import { FormGroup, FormBuilder, Validators } from '@angular/forms'; //add this
import { NgbDateNativeAdapter, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationService, Confirmation } from '@abp/ng.theme.shared';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  providers: [
    ListService,
    { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter } // add this line
  ],
})
export class BookComponent implements OnInit {
  book = { items: [], totalCount: 0 } as PagedResultDto<BookDto>;

  selectedBook = {} as BookDto;  // declare selectedBook

  form: FormGroup; // add this line to the

  authors$: Observable<AuthorLookupDto[]>;

  //add bookTypes as a list of BookType enum members
  bookTypes = bookTypeOptions;

  isModalOpen = false; //add this line

  constructor(
    public readonly list: ListService,
    private bookService: BookService,
    private fb: FormBuilder, // inject FormBuilder
    private confirmation: ConfirmationService // inject the ConfirmationService
    ) {
      this.authors$ = bookService.getAuthorLookup().pipe(map((r) => r.items));
    }

  ngOnInit() {
    const bookStreamCreator = (query) => this.bookService.getList(query);

    this.list.hookToQuery(bookStreamCreator).subscribe((response) => {
      this.book = response;
    });
  }

  // add new methods
  createBook(){
    this.selectedBook = {} as BookDto; // reset the selected book
    this.buildForm(); //add this line
    this.isModalOpen = true;
  }

  // Add editBook method
  editBook(id: string){
    this.bookService.get(id).subscribe((book) => {
      this.selectedBook = book;
      this.buildForm();
      this.isModalOpen = true;
    });
  }

  // add buildForm methods
  buildForm() {
    this.form = this.fb.group({
      authorId: [this.selectedBook.authorId || null, Validators.required],
      name: [this.selectedBook.name || null, Validators.required],
      type: [this.selectedBook.type || null, Validators.required],
      publishDate: [
        this.selectedBook.publishDate ? new Date(this.selectedBook.publishDate) : null,
         Validators.required
        ],
      price: [this.selectedBook.price || null, Validators.required],
    });
  }



  //add save methods
  save(){
    if (this.form.invalid) {
      return;
    }

    try{
      const request = this.selectedBook.id
      ? this.bookService.update(this.selectedBook.id, this.form.value)
      : this.bookService.create(this.form.value);

      request.subscribe(() => {
        this.isModalOpen = false;
        this.form.reset();
        this.list.get();
      });
    }
    catch (err) {
      console.log(err);
    }



  }

  //Add a delete method
  delete(id: string) {
    this.confirmation.warn('::AreYouSureToDelete', '::AreYouSure').subscribe((status) => {
      if(status === Confirmation.Status.confirm){
        this.bookService.delete(id).subscribe(() => this.list.get());
      }
    });
  }
}
