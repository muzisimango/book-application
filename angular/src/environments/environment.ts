import { Environment } from '@abp/ng.core';

const baseUrl = 'http://localhost:4200';

export const environment = {
  production: false,
  application: {
    baseUrl,
    name: 'BookStoreTutorial',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44315',
    redirectUri: baseUrl,
    clientId: 'BookStoreTutorial_App',
    responseType: 'code',
    scope: 'offline_access BookStoreTutorial',
    requireHttps: true,
  },
  apis: {
    default: {
      url: 'https://localhost:44315',
      rootNamespace: 'BookStoreTutorial',
    },
  },
} as Environment;
