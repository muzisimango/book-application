﻿using Volo.Abp.Localization;

namespace BookStoreTutorial.Localization;

[LocalizationResourceName("BookStoreTutorial")]
public class BookStoreTutorialResource
{

}
