﻿namespace BookStoreTutorial;

public static class BookStoreTutorialDomainErrorCodes
{
    /* You can add your business exception error codes here, as constants */
    public const string AuthorAlreadyExists = "BookStore:00001";
}
