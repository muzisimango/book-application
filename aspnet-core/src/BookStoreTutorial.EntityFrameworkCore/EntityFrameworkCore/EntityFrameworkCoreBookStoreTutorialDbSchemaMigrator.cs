﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using BookStoreTutorial.Data;
using Volo.Abp.DependencyInjection;

namespace BookStoreTutorial.EntityFrameworkCore;

public class EntityFrameworkCoreBookStoreTutorialDbSchemaMigrator
    : IBookStoreTutorialDbSchemaMigrator, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public EntityFrameworkCoreBookStoreTutorialDbSchemaMigrator(
        IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task MigrateAsync()
    {
        /* We intentionally resolving the BookStoreTutorialDbContext
         * from IServiceProvider (instead of directly injecting it)
         * to properly get the connection string of the current tenant in the
         * current scope.
         */

        await _serviceProvider
            .GetRequiredService<BookStoreTutorialDbContext>()
            .Database
            .MigrateAsync();
    }
}
