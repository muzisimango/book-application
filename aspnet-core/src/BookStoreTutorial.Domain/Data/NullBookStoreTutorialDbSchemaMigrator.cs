﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace BookStoreTutorial.Data;

/* This is used if database provider does't define
 * IBookStoreTutorialDbSchemaMigrator implementation.
 */
public class NullBookStoreTutorialDbSchemaMigrator : IBookStoreTutorialDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
