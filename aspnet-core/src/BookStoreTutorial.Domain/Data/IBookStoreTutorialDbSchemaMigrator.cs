﻿using System.Threading.Tasks;

namespace BookStoreTutorial.Data;

public interface IBookStoreTutorialDbSchemaMigrator
{
    Task MigrateAsync();
}
