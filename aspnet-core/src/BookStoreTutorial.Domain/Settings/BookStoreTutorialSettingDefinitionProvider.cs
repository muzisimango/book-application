﻿using Volo.Abp.Settings;

namespace BookStoreTutorial.Settings;

public class BookStoreTutorialSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(BookStoreTutorialSettings.MySetting1));
    }
}
