﻿using BookStoreTutorial.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace BookStoreTutorial.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class BookStoreTutorialController : AbpControllerBase
{
    protected BookStoreTutorialController()
    {
        LocalizationResource = typeof(BookStoreTutorialResource);
    }
}
