﻿using AutoMapper;
using BookStore.Books;
using BookStoreTutorial.Authors;
using BookStoreTutorial.Books;

namespace BookStoreTutorial;

public class BookStoreTutorialApplicationAutoMapperProfile : Profile
{
    public BookStoreTutorialApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
        CreateMap<Book, BookDto>();
        CreateMap<CreateUpdateBookDto, Book>();
        CreateMap<Author, AuthorDto>();
        CreateMap<Author, AuthorLookupDto>();
    }
}
