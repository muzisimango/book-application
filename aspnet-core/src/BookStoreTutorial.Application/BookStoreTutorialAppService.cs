﻿using System;
using System.Collections.Generic;
using System.Text;
using BookStoreTutorial.Localization;
using Volo.Abp.Application.Services;

namespace BookStoreTutorial;

/* Inherit your application services from this class.
 */
public abstract class BookStoreTutorialAppService : ApplicationService
{
    protected BookStoreTutorialAppService()
    {
        LocalizationResource = typeof(BookStoreTutorialResource);
    }
}
