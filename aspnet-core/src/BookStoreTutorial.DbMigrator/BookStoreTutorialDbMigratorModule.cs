﻿using BookStoreTutorial.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace BookStoreTutorial.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(BookStoreTutorialEntityFrameworkCoreModule),
    typeof(BookStoreTutorialApplicationContractsModule)
    )]
public class BookStoreTutorialDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
