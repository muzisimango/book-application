﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace BookStoreTutorial;

[Dependency(ReplaceServices = true)]
public class BookStoreTutorialBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "BookStoreTutorial";
}
