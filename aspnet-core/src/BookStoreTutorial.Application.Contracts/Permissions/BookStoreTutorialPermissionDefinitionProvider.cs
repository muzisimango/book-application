﻿using BookStoreTutorial.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace BookStoreTutorial.Permissions;

public class BookStoreTutorialPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var bookStoreTutorialGroup = context.AddGroup(BookStoreTutorialPermissions.GroupName, L("Permission:BookStore"));
        //Define your own permissions here. Example:
        //myGroup.AddPermission(BookStoreTutorialPermissions.MyPermission1, L("Permission:MyPermission1"));
        var booksPermission = bookStoreTutorialGroup.AddPermission(BookStoreTutorialPermissions.Books.Default, L("Permission:Books"));
        booksPermission.AddChild(BookStoreTutorialPermissions.Books.Create, L("Permission.Books.Create"));
        booksPermission.AddChild(BookStoreTutorialPermissions.Books.Edit, L("Permission.Books.Edit"));
        booksPermission.AddChild(BookStoreTutorialPermissions.Books.Delete, L("Permission.Books.Delete"));

        // new lines

        var authorsPermission = bookStoreTutorialGroup.AddPermission(
            BookStoreTutorialPermissions.Authors.Default, L("Permission:Authors"));

        authorsPermission.AddChild(
            BookStoreTutorialPermissions.Authors.Create, L("Permission:Authors.Create"));
        authorsPermission.AddChild(
            BookStoreTutorialPermissions.Authors.Edit, L("Permission:Authors.Edit"));
        authorsPermission.AddChild(
            BookStoreTutorialPermissions.Authors.Delete, L("Permission:Authors.Delete"));
    }
    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<BookStoreTutorialResource>(name);
    }
}
