﻿using Volo.Abp.Modularity;

namespace BookStoreTutorial;

[DependsOn(
    typeof(BookStoreTutorialApplicationModule),
    typeof(BookStoreTutorialDomainTestModule)
    )]
public class BookStoreTutorialApplicationTestModule : AbpModule
{

}
