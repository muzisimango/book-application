﻿using BookStoreTutorial.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace BookStoreTutorial;

[DependsOn(
    typeof(BookStoreTutorialEntityFrameworkCoreTestModule)
    )]
public class BookStoreTutorialDomainTestModule : AbpModule
{

}
